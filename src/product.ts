import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productRepository = AppDataSource.getRepository(Product);

    // Add new data
    // const product = new Product();
    // product.name = "ข้าวหน้าเป็ด"
    // product.price = 50;
    // await productRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    console.log("Loading products from the database...")
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    // update data @Product
    const updateProduct = await productRepository.findOneBy({id: 1})
    console.log(updateProduct)
    updateProduct.price = 80
    await productRepository.save(updateProduct)

    // console.log("Here you can setup and run express / fastify / any other framework.")

}).catch(error => console.log(error))
